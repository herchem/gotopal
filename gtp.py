import sys

from PIL import Image
from PIL import ImageFilter
from PIL import ImageEnhance

from skimage.transform import rescale
from skimage.color import convert_colorspace
from skimage.color import rgb2lab, lab2rgb, rgb2hsv, lab2lch, lch2lab
from skimage import img_as_float
from skimage.viewer import ImageViewer
from skimage import feature
from skimage import exposure
from skimage import filters as FILTRO

from PIL.Image import fromarray as toimage
# replaces :from scipy.misc import toimage

from scipy import optimize
from scipy import ndimage

from imageio import imwrite as imsave
# replaces :from scipy.misc import imsave

import numpy as np


def give_help():
    print("Usage:\n  python gtp.py [OPTIONS]\n\nMandatory:\n  -i\t--input\t\t<input_file_name>\n"+
         "  -o\t--output\t<output_file_name>\n"+
         "  -p\t--pallete\t[monochrome; complementary; analogous; triad; tetradic;\n"+
         "\t\t\tsplitcomplementary; square; quintet; sextet; rainbow; twelve]\n"+
         "Optional:\n"+
         "  -cs\t--color-space\tColor space: 'LAB' or 'HCL' ('HCL' default)\n" 
         "  -h\t--help\t\tPrints this help message\n\n"+
         "Example:\n     python gtp.py -i my_photo.jpg  -o modified_photo.png -p complementary -cs 'LAB'\n")
    exit()
    return


if len(sys.argv) == 1:
    give_help()

for k, arg in enumerate(sys.argv):
    if arg in ['-i', '--input'] and len(sys.argv) > k + 1:
        input_image = sys.argv[k + 1]
        del sys.argv[k]
        del sys.argv[k]
        break

for k, arg in enumerate(sys.argv):
    if arg in ['-o', '--output'] and len(sys.argv) > k + 1:
        output_image = sys.argv[k + 1]
        del sys.argv[k]
        del sys.argv[k]
        break

for k, arg in enumerate(sys.argv):
    if arg in ['-p', '--pallete'] and len(sys.argv) > k + 1:
        tipo_paleta = sys.argv[k + 1]
        del sys.argv[k]
        del sys.argv[k]
        break

supplied_color_space = "HCL" # Defaults to lab
for k, arg in enumerate(sys.argv):
    if arg in ['-cs', '--color-space'] and len(sys.argv) > k + 1:
        supplied_color_space = sys.argv[k + 1].upper()
        del sys.argv[k]
        del sys.argv[k]
        if supplied_color_space not in ["LAB", "HCL"]:
            give_help()
            exit()
        break

for k, arg in enumerate(sys.argv):
    if arg in ['-h', '--help']:
        give_help()
        break



try:
    input_image
except NameError:
    print("ERROR! No imput image was given!\n")
    give_help()
    exit()

try:
    output_image
except NameError:
    print("ERROR! No output name was given\n")
    give_help()
    exit()

try:
    tipo_paleta
except NameError:
    print("ERROR! No palette type was given\n")
    give_help()
    exit()







# Generic color palettes.

def pal_monochrome(x):
    return((x,))

def pal_complementary(x):
    return(x, divmod(x + 0.5, 1)[1])

def pal_analogous(x):
    return(x, divmod(x + 1./12., 1)[1], divmod(x + 2./12., 1)[1])

def pal_triad(x):
    return(x, divmod(x + 1./3., 1)[1], divmod(x + 2./3., 1)[1])

def pal_split_compl(x):
    return(x, divmod(x + 2./12., 1)[1], divmod(x + 7./12., 1)[1])

def pal_tetradic(x):
    return(x, divmod(x + 2./12., 1)[1], divmod(x + 6./12., 1)[1],divmod(x + 8./12., 1)[1])

def pal_square(x):
    return(x, divmod(x + 3./12., 1)[1], divmod(x + 6./12., 1)[1],divmod(x + 9./12., 1)[1])

def pal_quintet(x):
    return(x, divmod(x + 2./10., 1)[1], divmod(x + 4./10., 1)[1],divmod(x + 6./10., 1)[1],divmod(x + 8./10., 1)[1])

def pal_sextet(x):
    return(x, divmod(x + 1./6., 1)[1], divmod(x + 2./6., 1)[1],divmod(x + 3./6., 1)[1],divmod(x + 4./6., 1)[1],divmod(x + 5./6., 1)[1])

def pal_rainbow(x):
    return(x, divmod(x + 1./7., 1)[1], divmod(x + 2./7., 1)[1],divmod(x + 3./7., 1)[1],divmod(x + 4./7., 1)[1],divmod(x + 5./7., 1)[1],divmod(x + 6./7., 1)[1])

def pal_doce(x):
    return(x, divmod(x + 1./12., 1)[1], divmod(x + 2./12., 1)[1],divmod(x + 3./12., 1)[1],divmod(x + 4./12., 1)[1],divmod(x + 5./12., 1)[1],divmod(x + 6./12., 1)[1],divmod(x + 7./12., 1)[1], divmod(x + 8./12., 1)[1],divmod(x + 9./12., 1)[1],divmod(x + 10./12., 1)[1],divmod(x + 11./12., 1)[1])

pal_points = {
    "monochrome"         : pal_monochrome,
    "complementary"      : pal_complementary,
    "analogous"          : pal_analogous, 
    "triad"              : pal_triad,
    "tetradic"           : pal_tetradic,
    "splitcomplementary" : pal_split_compl,
    "square"             : pal_square,
    "quintet"            : pal_quintet,
    "sextet"             : pal_sextet,
    "rainbow"            : pal_rainbow,
    "twelve"             : pal_doce,
}



# Import image

im_orig_rgb = Image.open(input_image)

# Compute the hue distance between an image 'h' (n*m array)
# and some reference (image or float in range 0-1)
def f_distance(h, ref):
    if isinstance(ref, float):
        ref = np.ones((h.shape[0],h.shape[1]))*ref 
    aux0 = ref - h
    aux1 = aux0 < -0.5
    aux0[aux1] = aux0[aux1] + 1.
    aux1 = aux0 > 0.5
    aux0[aux1] = aux0[aux1] - 1.
    return(abs(aux0))

# counterclockwise distance (angle in rads / (2pi))
# from h to ref
def f_dist_acw(h, ref):
    if isinstance(ref, float):
        ref = np.ones(len(h))*ref 
    aux0 = ref - h
    aux1 = aux0 < 0.
    aux0[aux1] = aux0[aux1] + 1.
    return(aux0)

# clockwise...
def f_dist_cw(h, ref):
    if isinstance(ref, float):
        ref = np.ones(len(h))*ref 
    aux0 = h - ref
    aux1 = aux0 < 0.
    aux0[aux1] = aux0[aux1] + 1.
    return(aux0)


#   Compute the cost function
def cost_function(x, h, s, l, style = "Square"):
    cost = 0.
    p = pal_points[style](x)
    costs = np.zeros( (len(p),len(h)))
    loss = 0.
    for i in range(costs.shape[0]):
        diff = np.abs(h - p[i])
        aux = np.minimum( diff, 1.- diff)
        aux[np.where(aux<1./12.)] = 0.
        costs[i,:] = - np.exp(-aux*12.*np.log(2.))+1.
    loss += (np.min(costs,axis=0)*s*abs((l-0.5))).sum()
    return(loss)


im_orig_rgb = img_as_float(im_orig_rgb)
im_lab   = rgb2lab(im_orig_rgb) 
im_lchab = lab2lch(im_lab)


im_l = im_lchab[:,:,0].flatten() / 100.
im_s = im_lchab[:,:,1].flatten() / 100. 
im_h = im_lchab[:,:,2].flatten() / (2.*np.pi) # normalization

original_shape = im_lchab[:,:,0].shape



def get_base_color(im_h, im_s, im_l, tipo_paleta):
    print("Searching for the best pallete of kind: ",tipo_paleta)
    n_pix = original_shape[0]*original_shape[1]
    if n_pix > 800*600:
       print("... The image is larger than 800x600")
       print("    Performing search on a reduced size image for increasing speed")
       small_rgb = rescale(im_orig_rgb, np.sqrt(800*600/n_pix),mode='reflect')
       print("jeje")
       small_lchab = lab2lch(rgb2lab(small_rgb))
       small_l = small_lchab[:,:,0].flatten() / 100.
       small_s = small_lchab[:,:,1].flatten() / 100.
       small_h = small_lchab[:,:,2].flatten() / (2.*np.pi)
       print("jiji")
       x = optimize.brute(cost_function,  (slice(0, 1, 0.01),), args=(small_h, small_s, small_l, tipo_paleta,), full_output=False, finish=optimize.fmin)[0]
    else:
       x = optimize.brute(cost_function,  (slice(0, 1, 0.01),), args=(im_h, im_s, im_l,tipo_paleta,), full_output=False, finish=optimize.fmin)[0]
    print("x optimum for palette type "+tipo_paleta, x)
    return(x)

x = get_base_color(im_h, im_s, im_l, tipo_paleta)



print("Computing target colors")
# Clockwise distance to the closest x[i]
min_cw = np.zeros( (len(pal_points[tipo_paleta](0.1)),len(im_h))  ) 
for i in range(min_cw.shape[0]):
    min_cw[i,:] = f_dist_cw(im_h, pal_points[tipo_paleta](x)[i])
min_cw = np.min(min_cw,axis=0)

# Counter-clockwise distance to the closest x[i]
min_acw = np.zeros( (len(pal_points[tipo_paleta](0.1)),len(im_h))  ) 
for i in range(min_acw.shape[0]):
    min_acw[i,:] = f_dist_acw(im_h, pal_points[tipo_paleta](x)[i])
min_acw = np.min(min_acw,axis=0)



# TARGET COLORS FOR EACH IMAGE POINT
# (looks for the closest x[i] colors)
objetives = np.zeros(len(min_cw))
# If the distance to some x[i] is lower in clockwise
cw =  min_cw < min_acw
objetives[cw] = im_h[cw] - min_cw[cw]
# else...
acw = min_cw >= min_acw
objetives[acw] = im_h[acw] + min_acw[acw]
# corrects out of range values
oor = objetives > 1.
objetives[oor] = objetives[oor] -1.
oor = objetives < 0.
objetives[oor] = objetives[oor] +1.


# *** *** *** ** ** *** *** *** #
# *** *** INTERPOLATION *** *** #
# *** *** *** ** ** *** *** *** #
# The target colors are already found. The original colors need to be biased to the target colors.
# Different color spaces provide different results
# e.gs: http://howaboutanorange.com/blog/2011/08/10/color_interpolation/
interpolation_type = supplied_color_space

if interpolation_type == "HCL":
# HCL Interpolation
# --------------------
# Chroma is conserved
# Hue distance between original and target
    distances = f_distance(im_h, objetives)
    direction = np.ones(len(im_h))
    direction[cw] = -direction[cw]

    max_d_vec = (min_cw + min_acw)*0.5   # max hue distance (in rad)
    dist_max = distances.max()

    new_h = im_h+ distances * direction*np.exp( -0.7*(max_d_vec-distances)/max_d_vec)


    aux0 = new_h > 1.
    new_h[aux0] = new_h[aux0] - 1.
    aux0 = new_h < 0.
    new_h[aux0] = new_h[aux0] + 1.

    new_h = new_h * (2.*np.pi)  # change to 0-360 for conversion
    im_lchab[:,:,2] = new_h.reshape(original_shape)
    im_orig_rgb = lab2rgb(lch2lab(im_lchab))
    imsave(output_image, im_orig_rgb) # save in RGB
    viewer = ImageViewer(im_orig_rgb)
    viewer.show()
elif interpolation_type == "LAB":
# Lab interpolation
# --------------------
    im_a = im_lab[:,:,1].flatten()
    im_b = im_lab[:,:,2].flatten()

    im_lchab_obj = np.copy(im_lchab)
    im_lchab_obj[:,:,2] = objetives.reshape(original_shape)* (2.*np.pi) 
    im_lab_obj   = lch2lab(im_lchab_obj)
    im_a_obj = im_lab_obj[:,:,1].flatten()
    im_b_obj = im_lab_obj[:,:,2].flatten()

    distances = f_distance(im_h, objetives)
    max_d_vec = (min_cw + min_acw)*0.5   # distancia maxima en angulo de hue

    beta = 0.5
    im_a_final = im_a * beta + im_a_obj*(1.-beta)
    im_b_final = im_b * beta + im_b_obj*(1.-beta)


    im_lab_obj[:,:,1] = im_a_final.reshape(original_shape)
    im_lab_obj[:,:,2] = im_b_final.reshape(original_shape)
    im_rgb_obj = lab2rgb(im_lab_obj)
    viewer = ImageViewer(im_rgb_obj)
    viewer.show()
    imsave(output_image, im_rgb_obj) # save in RGB


