# Introduction

GTP is a [Python3](https://www.python.org/) program that biases the colors of any image to adequate them to some [color scheme type](https://en.wikipedia.org/wiki/Color_scheme#Types).

To get an idea of the results you can expect to see the image below.

![sample](https://i.ibb.co/MM8hK0v/sample.jpg)



# Installation and usage

No installation is required. Just copy the `gtp.py` file and run it using a Python3 interpreter.

Some arguments must be given as is explained in the help message

```
my-user@my-desktop:/somedir$ python gtp.py 
Usage:
  python gtp.py [OPTIONS]

Mandatory:
  -i    --input         <input_file_name>
  -o    --output        <output_file_name>
  -p    --pallete       [monochrome; complementary; analogous; triad; tetradic;
                        splitcomplementary; square; quintet; sextet; rainbow; twelve]
Optional:
  -cs   --color-space   Color space: 'LAB' or 'HCL' ('HCL' default)
  -h    --help          Prints this help message

Example:
     python gtp.py -i my_photo.jpg  -o modified_photo.png -p complementary -cs 'LAB'

```

# How does it works?
The user supplies an image and a color scheme type. If you do not know what it is, see [this](https://www.tigercolor.com/color-lab/color-theory/color-harmonies.htm
) link. GTP finds which colors compose the palette of the supplied type that best fits the provided image. Best is not a mathematical objective term, here it means that it minimizes some cost function. Those colors are the target colors in the sense that the image color will be biased towards them.
For each point of the image, the closest target color is found. The new and final color of the point is found through interpolation between the original and target color. In order to reduce artifacts, a damping function is used.

The color interpolation depends on the [color space](https://en.wikipedia.org/wiki/Color_space). Some color spaces provide more 'natural' color interpolation, see [this](https://web.archive.org/web/20180522085923/http://howaboutanorange.com/blog/2011/08/10/color_interpolation/) link. Currently, Lab and HCL color spaces are supported.





# Licence
**GTP**

Copyright (C) 2019 Hernán Sánchez

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see https://www.gnu.org/licenses/.

hernan.sanchez.ds@gmail.com
